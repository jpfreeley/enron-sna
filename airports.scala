// Databricks notebook source
# from tutorial here: http://sparktutorials.net/analyzing-flight-data:-a-gentle-introduction-to-graphx-in-spark
# data download available here: http://stat-computing.org/dataexpo/2009/the-data.html

import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import scala.util.MurmurHash
val df_1 = sqlContext.read.format("com.databricks.spark.csv").option("header", "true").load("/FileStore/tables/airports/2008.csv")

// COMMAND ----------

val flightsFromTo = df_1.select($"Origin",$"Dest")
val airportCodes = df_1.select($"Origin", $"Dest").flatMap(x => Iterable(x(0).toString, x(1).toString))



// COMMAND ----------

flightsFromTo.show()

// COMMAND ----------

val airportVertices = airportCodes.distinct().map(x => (MurmurHash.stringHash(x), x))
val defaultAirport = ("Missing")

// COMMAND ----------

airportVertices.show()

// COMMAND ----------

val flightEdges = flightsFromTo.map(x => ((MurmurHash.stringHash(x(0).toString),MurmurHash.stringHash(x(1).toString)), 1))
flightEdges.createOrReplaceTempView("df")
val fe = sqlContext.sql("SELECT _1, SUM(_2) AS _2 FROM df GROUP BY _1").map(x => x.toString.split(",")).map( y=> Edge(y(0).replace("[","").toInt,y(1).replace("]","").toInt,y(2).replace("]","").toInt))
fe.show(10, false)


// COMMAND ----------

val graph = Graph(airportVertices, flightEdges, defaultAirport)
graph.persist() // we're going to be using it a lot
