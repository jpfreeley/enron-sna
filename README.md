# enron-communities
SJU-CUS-681 SNA of Enron Email Corpus

## configuration
add your personal Enron Corpus upload to the config.py location

## gitlab setup
1. in commandline, navigate to the folder you want to store the project in.
2. ``` git clone https://gitlab.com/lilyz622/enron-sna.git ```
3. ``` cd enron-sna ```
4. ``` git init ```
5. ``` git remote add origin https://gitlab.com/lilyz622/enron-sna.git ```
6. ``` git remote add master https://gitlab.com/lilyz622/enron-sna.git ```
7. ``` git pull origin master ```
8. ``` git checkout -b myFeature ```

## to make changes

``` git add . ```
``` git commit -m "description of changes" ```
``` git push origin myFeature ```