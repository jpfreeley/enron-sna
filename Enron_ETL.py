# Databricks notebook source
# KEPT IN CODEBASE AS EXMAPLE OF LIBRARY IMPORT -- NOT CURRENTLY USED

# In order to work with the NLTK library do the following:
# 1. In the Workspace create a library by select a folder, then click on the dropdown->Create->Library
# 2. In the New Library Screen select Python as language
# 3. As Pypi name specify "nltk" as the Pypi name

#import nltk

# COMMAND ----------

# MAGIC %run ./config

# COMMAND ----------

# raw_file_location is a value read in from config.py
raw_file = sc.textFile(raw_file_location)
raw_file.take(5) # demonstrate that file was properly imported

# COMMAND ----------

# parse employeeList and create eldf fraom with 2 columns: int("ID"),char("EMAIL")
# grab all lines that match employeelist insertions
# replace all spaces, replace all single-quotes, split on commas
# create an array:
#   element 0 is the integer split after parentheses of element 0 above,
#   element 1 is the email address (element 3 above)
el = raw_file.filter(
  lambda line: "INSERT INTO employeelist VALUES" in line). \
  map(lambda line: line.replace(" ","").replace("'","").split(",")). \
  map(lambda array: (int(array[0].split("(")[1]),array[3][0:-2]))

#create spark DF from the 2 columnms parsed above.
eldf = sqlContext.createDataFrame(el,["id","email"])

eldf.describe().show()
eldf.show(5)
eldf.printSchema()

# COMMAND ----------

# parse "message" and create "msgdf" fraom with 2 columns: int("MID"),char("SENDER")
# grab all lines that match message insertions
# replace all spaces, replace all single-quotes, split on commas
# create an array:
#   element 0 is the integer split after parentheses of element 0 above,
#   element 1 is the email address (element 1 above)
msg = raw_file.filter(
  lambda line: "INSERT INTO message VALUES" in line). \
  map(lambda line: line.replace(" ","").replace("'","").split(",")). \
  map(lambda array: (int(array[0].split("(")[1]),array[1][0:]))

#create spark DF from the 2 columnms parsed above.
msgdf=sqlContext.createDataFrame(msg,["mid","sender"])

msgdf.describe().show()
msgdf.show(5)
msgdf.printSchema()

# COMMAND ----------

# KEPT IN CODEBASE AS EXAMPLE OF FUNCTION -- THIS IS NOT CURRENTLY USED
# msg = Message Table
def cleanString(arrayString):
  result = []
  for s in arrayString:
    if (not s.isdigit()):
      result.append(s[:-1])
    else:
      result.append(s)
  return result

# COMMAND ----------

# parse "recipientinfo" and create "ridf" fraom with 2 columns: int("MID"),char("RTYPE"),char("SENDER")
# grab all lines that match message insertions
# replace all spaces, replace all single-quotes, split on commas
# create an array:
#   element 0 is the integer of element 1 above,
#   element 1 is the rtype address (element 2 above)
#   element 2 is the email address (element 3 above)

recipInfo = raw_file.filter(
  lambda line: "INSERT INTO recipientinfo VALUES" in line). \
  map(lambda line: line.replace(" ","").replace("'","").split(",")). \
  map(lambda array: (int(array[1]),array[2],array[3]))

#create spark DF from the 3 columnms parsed above.
ridf=sqlContext.createDataFrame(recipInfo,["mid","rtype","remail"])

ridf.describe().show()
ridf.show(5)
ridf.printSchema()

# COMMAND ----------

# join ridf and msgdf on mid results in 4 columns: 
# mid, rtype, remail, sender
resultDf = ridf.join(msgdf,"mid")
resultDf.show(25)
