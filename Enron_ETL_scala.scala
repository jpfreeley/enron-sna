// Databricks notebook source
// MAGIC %md # Enron Network Analysis
// MAGIC Welcome to the Enron Analysis notebook, broken down into the following sections:
// MAGIC - ETL
// MAGIC - Graph Analysis

// COMMAND ----------

// MAGIC %md ### ETL
// MAGIC where we:
// MAGIC - extract the data from the sql file
// MAGIC - clean the data
// MAGIC - transform the data into rdds and into vertices and edges
// MAGIC - load the data into a graph
// MAGIC 
// MAGIC result:
// MAGIC - eldf: 
// MAGIC   - "employee list" : one element per employee of enron
// MAGIC   - dataframe with fields: 
// MAGIC     - id:integer
// MAGIC     - firstName:string 
// MAGIC     - lastName:string 
// MAGIC     - email:string
// MAGIC - msgdf:
// MAGIC   - "messages" : one element per message
// MAGIC   - dataframe with fields: 
// MAGIC     - mid:integer
// MAGIC     - sender:string
// MAGIC - ridf:
// MAGIC   - "recipient info" : one element per email per recipient
// MAGIC   - dataframe with fields: 
// MAGIC     - mid:integer the id of the message
// MAGIC     - rtype:string the label of the receiver of the email (eg. CC, TO, BCC)
// MAGIC     - remail:string the email address of the receiver
// MAGIC - resultDf:
// MAGIC   - "new messages df" that joins ridf and msgdf on mid results in 4 columns : one element per email per recipient WITH SENDER
// MAGIC   - dataframe with fields: 
// MAGIC     - mid:integer the id of the message
// MAGIC     - rtype:string the label of the receiver of the email (eg. CC, TO, BCC)
// MAGIC     - remail:string the email address of the receiver
// MAGIC     - sender:string the email address of the sender
// MAGIC - allEmailAddresses:
// MAGIC   - "all email addresses" : one element per unique email address (of all the employees and their correspondents)
// MAGIC   - dataset with fields:
// MAGIC     - email:string
// MAGIC     - hash:long the hash value of the email
// MAGIC - graph:
// MAGIC   - "social graph" 
// MAGIC   - Edge[srcId: Long, dstId: Long, attr: Int]:
// MAGIC     - srcId: the hashed email address of the sender
// MAGIC     - dstId: the hashed email address of the recipient
// MAGIC     - attr: the weight of the edge (the number of emails sent from sender to recipient)
// MAGIC   - Vertex(VertexId: Long, EmailAddress: EmailAddress):
// MAGIC     - a tuple

// COMMAND ----------

import scala.util.MurmurHash
import scala.util.hashing.MurmurHash3
import org.apache.spark.rdd.RDD
import org.apache.spark.graphx._
import scala.collection.mutable.HashMap

// COMMAND ----------

// MAGIC %run ./config

// COMMAND ----------

val raw_file = sc.textFile(raw_file_location)
raw_file.take(5) // demonstrate that file was properly imported

// COMMAND ----------

// parse employeeList and create eldf fraom with 2 columns: int("ID"),char("EMAIL")
// grab all lines that match employeelist insertions
// replace all spaces, replace all single-quotes, split on commas
// create an array

case class Employee(id: Int, firstName: String, lastName:String, email: String, hash: Long)

val el = raw_file.filter(line => line.contains("INSERT INTO employeelist VALUES")).map(line => line.replaceAll("\\s","").split("\\(")(1).split("\\);")(0).split(",")).map(line=>Employee(line(0).toInt,line(1),line(2),line(3),MurmurHash.stringHash(line(3)).toLong))

// create spark DF from the 2 columnms parsed above.
val eldf = sqlContext.createDataFrame(el)

eldf.describe().show()
eldf.show(5)
eldf.printSchema()

// COMMAND ----------

// # parse "message" and create "msgdf" from with 2 columns: int("MID"),char("SENDER")
// # grab all lines that match message insertions
// # replace all spaces, replace all single-quotes, split on commas
// # create an array:
// #   element 0 is the integer split after parentheses of element 0 above,
// #   element 1 is the email address (element 1 above)

case class Message(mid: Int, sender: String)

val msg = raw_file.filter(line => line.contains("INSERT INTO message VALUES")).map(line => line.replaceAll("\\s","").split("\\(")(1).split("\\);")(0).split(",")).map(line=>Message(line(0).toInt,line(1)))

// create spark DF from the 2 columnms parsed above.
val msgdf = sqlContext.createDataFrame(msg)

msgdf.describe().show()
msgdf.show(5)
msgdf.printSchema()


// COMMAND ----------

// # parse "recipientinfo" and create "ridf" fraom with 2 columns: int("MID"),char("RTYPE"),char("SENDER")
// # grab all lines that match message insertions
// # replace all spaces, replace all single-quotes, split on commas
// # create an array:
// #   element 0 is the integer of element 1 above,
// #   element 1 is the rtype address (element 2 above)
// #   element 2 is the email address (element 3 above)

case class RecipientInfo(mid:Int, rtype:String, remail:String)

val recipInfo = raw_file.filter(line => line.contains("INSERT INTO recipientinfo VALUES")).map(line => line.replaceAll("\\s","").split("\\(")(1).split("\\);")(0).split(",")).map(line=>RecipientInfo(line(1).toInt, line(2), line(3)))

//create spark DF from the 3 columns parsed above
val ridf = sqlContext.createDataFrame(recipInfo)

ridf.describe().show()
ridf.show(5)
ridf.printSchema()

// COMMAND ----------

// join ridf and msgdf on mid results in 4 columns: 
// mid, rtype, remail, sender
val resultDf = ridf.join(msgdf, "mid")
resultDf.show(25)

// COMMAND ----------

// create dataset of distinct email addresses to be used as vertices

case class EmailAddressHash(email: String, hash:Long)

// create a fast hash/address lookup table
val allEmailAddresses = resultDf.flatMap(x=>Iterable(x(2).toString,x(3).toString)).distinct().map(x=>EmailAddressHash(x, MurmurHash.stringHash(x).toLong))


allEmailAddresses.describe().show()
allEmailAddresses.show(5)
allEmailAddresses.printSchema()

// COMMAND ----------

// MAGIC %md ### Creating a graph with all the emails

// COMMAND ----------

// MAGIC %md After running the following code that creates a graph composed of all the emails, I found that we had 75057 vertices, when we only have 151 employees. This means that the number of non-employees is to much to factor into when trying to identify Enron communities. For now, I have left the code in case we want to analyze contact outside of the employee circle later on.
// MAGIC ```
// MAGIC // create vertices and edges formatted to be put in the graph.
// MAGIC case class EmailAddress(email:String)
// MAGIC 
// MAGIC // create vertices of the form (id: Long, object: EmailAddress)
// MAGIC val emailVertices: RDD[(Long, EmailAddress)] = allEmailAddresses.map(x=>(x.hash, EmailAddress(x.email))).rdd
// MAGIC 
// MAGIC // defaultEmail in case edge points to missing vertex
// MAGIC val defaultEmail = (EmailAddress("Missing"))
// MAGIC 
// MAGIC // create edges of the form RDD[Edge(id1, id2, countOfEmails)]
// MAGIC // TODO: make relationships a hashmap with key as relationshipType and value as the count, (eg. key="TO", value=20)
// MAGIC val emailEdges: RDD[Edge[Int]] = resultDf
// MAGIC   .map(x=>((MurmurHash.stringHash(x(2).toString).toLong,MurmurHash.stringHash(x(3).toString).toLong), 1))
// MAGIC   .rdd
// MAGIC   .reduceByKey((a,b)=> a+b)
// MAGIC   .map(x=> Edge(x._1._1, x._1._2, x._2))
// MAGIC 
// MAGIC // create the graph using vertices and edges
// MAGIC val graph = Graph(emailVertices, emailEdges, defaultEmail)
// MAGIC graph.persist()
// MAGIC 
// MAGIC // make sure graph persisted correctly
// MAGIC val numEdges: Long = graph.numEdges
// MAGIC val numVertices: Long = graph.numVertices
// MAGIC // sum the "weight" of each edge to get a total of 2064442 emails, which is correct
// MAGIC val numEmails = graph.edges.reduce((x,y) => Edge(-1L,0L, (x.attr.toInt+y.attr.toInt)))
// MAGIC 
// MAGIC /*
// MAGIC RESULT: 
// MAGIC numEdges: Long = 322415
// MAGIC numVertices: Long = 75057
// MAGIC numEmails: org.apache.spark.graphx.Edge[Int] = Edge(-1,0,2064442)
// MAGIC */
// MAGIC 
// MAGIC // check for missing vertices - there are none
// MAGIC val numMissing = graph.vertices.filter(x => x._2.email == "Missing").count
// MAGIC ```

// COMMAND ----------

val employeeHashes = eldf.map(x=>(MurmurHash.stringHash(x.getAs[String]("email")).toLong)).collect()

// COMMAND ----------

// although there are only 151 employees, we have over 75,000 vertices ... 
// let's cut out the vertices that are non-employee
import scala.collection.immutable.HashSet

// 1. broadcast the list of employees

val employeeHashes = eldf.map(x=>(MurmurHash.stringHash(x.getAs[String]("email")).toLong)).rdd.collect()
val parameters = collection.immutable.HashMap({"employees" -> HashSet().++(employeeHashes)})
val bParameters = sc.broadcast(parameters)

bParameters.value.get("employees")

// 2. create the employee-only graph

// create vertices and edges formatted to be put in the graph.
case class EmailAddress(email:String)

// create vertices of the form (id: Long, object: EmailAddress)
val emailVertices: RDD[(Long, EmailAddress)] = eldf
  .map(x=>(MurmurHash.stringHash(x.getAs[String]("email")).toLong, EmailAddress(x.getAs[String]("email")))).rdd

// defaultEmail in case edge points to missing vertex
val defaultEmail = (EmailAddress("Missing"))

//create edges of the form RDD[Edge(id1, id2, countOfEmails)]
// TODO: make relationships a hashmap with key as relationshipType and value as the count, (eg. key="TO", value=20)
val emailEdgesRdd = resultDf
  .map(x=>((MurmurHash.stringHash(x(2).toString).toLong,MurmurHash.stringHash(x(3).toString).toLong), 1))
  .rdd
  .filter(x => bParameters.value.get("employees").get(x._1._1) && bParameters.value.get("employees").get(x._1._2))
  .reduceByKey((a,b)=> a+b)

val emailEdges = emailEdgesRdd.map(x=> Edge(x._1._1, x._1._2, x._2))

// 3. create the graph
val graph = Graph(emailVertices, emailEdges, defaultEmail)
graph.persist()


// 4. make sure graph persisted correctly

val numEdges: Long = graph.numEdges // 2235
val numVertices: Long = graph.numVertices //151

// check for missing vertices - there are none
val numMissing = graph.vertices.filter(x => x._2.email == "Missing").count // 0

// sum the "weight" of each edge to get the total number of inter-employee emails
val numEmails = graph.edges.reduce((x,y) => Edge(-1L,0L, (x.attr.toInt+y.attr.toInt))) // 50574

// COMMAND ----------

/* 
output to csv. To download the file:
1. go to data
2. click "add data"
3. click "DBFS"
4. navigate to Filestore/tables
5. copy the link eg. FileStore/tables/nodes.csv/part-00000-tid-2595115853338567611-a2cda32e-cfb5-4d04-ae1f-93be32850a58-4257-c000.csv
6. in your browser, go to https://community.cloud.databricks.com/files/<your link>
eg. https://community.cloud.databricks.com/files/tables/nodes.csv/part-00000-tid-2595115853338567611-a2cda32e-cfb5-4d04-ae1f-93be32850a58-4257-c000.csv
*/
// eldf.coalesce(1).write.csv("/FileStore/tables/eNodes")
// sqlContext.createDataFrame(emailEdges.map(x=>(x.srcId,x.dstId, x.attr))).coalesce(1).write.csv("/FileStore/tables/eEdges")

// COMMAND ----------



// COMMAND ----------

// MAGIC %md ### Graph Analysis
// MAGIC where we:
// MAGIC - apply the ... algorithm to determine ...

// COMMAND ----------

val inDegrees: VertexRDD[Int] = graph.inDegrees
val sortedInDegrees: RDD[(VertexId, Int)] = inDegrees.sortBy(x=>x._2, false)

// COMMAND ----------

// Jeff Dasovich is the director for state government affairs. He has the highest count of received emails at ~60,000
// graph.vertices.filter(x => x._1 == 1910493300).take(1)
